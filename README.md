# DSpace Registering Items

Crear csv con los ítems a registrar con el siguiente formato:

```
id,collection,dc.source,dc.title
+,112345/1234,https://dirección.documento.net/documento.pdf,Titulo del ítem
+,112345/1235,https://dirección.documento.net/documento2.pdf,Titulo del ítem 2
```

Ejecutar el siguiente comando:

```

/dspace/bin/dspace metadata-import -f /path/to/file/import.csv -e admin@user.com

```


## Agregar campo Source en página del item

Para que el usuario final pueda visualizar la URL del documento, se debe habilitar en el FrontEnd esta visualización.


Agregar en:

./src/app/item-page/simple/item-types/untyped-item/untyped-item.component.html

Las siguientes líneas:

```
<ds-item-page-uri-field [item]="object"
    [fields]="['dc.source']"
    [label]="'item.page.source'">
```

Agregar en:

./src/assets/i18n/es.json5 

La siguiente línea:

```
"item.page.source": "Enlace de descarga",
```

